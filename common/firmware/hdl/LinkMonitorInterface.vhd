library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
---
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.ipbus_decode_linkmonitorinterface.all;
---
use work.emp_data_types.all;
use work.dtc_constants.all;
use work.dtc_data_types.all;


entity LinkMonitorInterface is
    generic (
        N_LINKS : integer;
        BIN_WIDTH : integer := 32
    );
    port (
        --- Input Ports ---
        clk_p        : in std_logic;
        stubs        : in ldata(N_LINKS - 1 downto 0);
        headers      : in tCICHeaderArray(N_LINKS*cNumberOfCICs - 1 downto 0) := (others => ('0', (others => '0'), (others => '0'), (others => '0')));
        header_start : in std_logic_vector(N_LINKS*cNumberOfCICs - 1 downto 0) := (others => '0');
        --- IPBus Ports ---
        clk            : in    std_logic;
        rst            : in    std_logic;
        ipb_in         : in    ipb_wbus;
        ipb_out        : out   ipb_rbus
    );
end LinkMonitorInterface ;

architecture Behavorial of LinkMonitorInterface is

    constant INPUT_WIDTH : integer := 7;

    signal stubs_internal   : ldata(N_LINKS - 1 downto 0) := (others => LWORD_NULL);
    signal headers_internal : tCICHeaderArray(N_LINKS*cNumberOfCICs - 1 downto 0) := (others => ('0', (others => '0'), (others => '0'), (others => '0')));
    signal header_start_internal : std_logic_vector(N_LINKS*cNumberOfCICs - 1 downto 0) := (others => '0');

    -- IPBus fabric

    signal ipb_to_slaves              : ipb_wbus_array(N_SLAVES - 1 downto 0);
    signal ipb_from_slaves            : ipb_rbus_array(N_SLAVES - 1 downto 0);
    signal link_sel                   : ipb_reg_v(0 downto 0);
    signal ipb_chain                  : ipbdc_bus_array(N_LINKS downto 0);

begin

    -- Buffer input
    --==============================--
    pBuffer : process(clk_p)
    --==============================--
    begin
        if rising_edge(clk_p) then
            stubs_internal <= stubs;
            headers_internal <= headers;
            header_start_internal <= header_start;
        end if;
    end process pBuffer;

    --==============================--
    -- IPBus fabric
    --==============================--

    --==============================--
    fabric : entity work.ipbus_fabric_sel
    --==============================--
    generic map (
        NSLV      => N_SLAVES,
        SEL_WIDTH => IPBUS_SEL_WIDTH
    )
    port map (
        ipb_in          => ipb_in,
        ipb_out         => ipb_out,
        sel             => ipbus_sel_linkmonitorinterface(ipb_in.ipb_addr),
        ipb_to_slaves   => ipb_to_slaves,
        ipb_from_slaves => ipb_from_slaves
    );

    --==============================--
    channel_ctrl : entity work.ipbus_reg_v
    --==============================--
    generic map (
        N_REG => 1
    )
    port map (
        clk       => clk,
        reset     => rst,
        ipbus_in  => ipb_to_slaves(N_SLV_LINK_CTRL),
        ipbus_out => ipb_from_slaves(N_SLV_LINK_CTRL),
        q         => link_sel,
        qmask     => (0 => X"0000007f")
    );

    --==============================--
    link_select : entity work.ipbus_dc_fabric_sel
    --==============================--
    generic map (
        SEL_WIDTH => 7
    )
    port map (
        clk       => clk,
        rst       => rst,
        sel       => link_sel(0)(6 downto 0),
        ipb_in    => ipb_to_slaves(N_SLV_LINK),
        ipb_out   => ipb_from_slaves(N_SLV_LINK),
        ipbdc_out => ipb_chain(0),
        ipbdc_in  => ipb_chain(N_LINKS)
    );

    --==============================--
    genLinkMonitors : for i in 0 to N_LINKS - 1 generate
    --==============================--
    
        signal ipb_to_channel   : ipb_wbus;
        signal ipb_from_channel : ipb_rbus;

    begin

        --==============================--
        channel_node : entity work.ipbus_dc_node
        --==============================--
        generic map (
            I_SLV     => i,
            SEL_WIDTH => 7,
            PIPELINE  => false
        )
        port map (
            clk       => clk,
            rst       => rst,
            ipb_out   => ipb_to_channel,
            ipb_in    => ipb_from_channel,
            ipbdc_in  => ipb_chain(i),
            ipbdc_out => ipb_chain(i + 1)
        );

        --==============================--
        LinkMonitorInstance : entity work.LinkMonitor
        --==============================--
        generic map (
            bin_width => BIN_WIDTH,
            input_width => INPUT_WIDTH,
            data_offset => 19 - INPUT_WIDTH
        ) 
        port map (
            --- Input Ports ---
            clk_p => clk_p,
            stub => stubs_internal(i),
            header => headers_internal(cNumberOfCICs*(i+1) - 1 downto cNumberOfCICs*i),
            header_start => header_start_internal(cNumberOfCICs*(i+1) - 1 downto cNumberOfCICs*i),
            --- IPBus Ports ---
            clk => clk,
            rst => rst,
            ipb_in => ipb_to_channel,
            ipb_out => ipb_from_channel
        );

    end generate ; -- genLinkMonitors

end architecture ;