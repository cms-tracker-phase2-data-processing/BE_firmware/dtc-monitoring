library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
---
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.ipbus_decode_linkmonitor.all;
---
use work.emp_data_types.all;
use work.dtc_constants.all;
use work.dtc_data_types.all;


entity LinkMonitor is
    generic (
        bin_width : integer;
        input_width : integer;
        data_offset : integer
    );
    port (
        --- Input Ports ---
        clk_p : in std_logic;
        stub  : in lword;
        header : in tCICHeaderArray(cNumberOfCICs - 1 downto 0);
        header_start : in std_logic_vector(cNumberOfCICs - 1 downto 0); 
        --- IPBus Ports ---
        clk            : in    std_logic;
        rst            : in    std_logic;
        ipb_in         : in    ipb_wbus;
        ipb_out        : out   ipb_rbus
    );
end LinkMonitor;

architecture Behavorial of LinkMonitor is

    constant N_CTRL_HIST              : integer := 4;
    constant N_STAT_HIST              : integer := 4;

    signal ipb_to_slaves              : ipb_wbus_array(N_SLAVES - 1 downto 0);
    signal ipb_from_slaves            : ipb_rbus_array(N_SLAVES - 1 downto 0);

    signal status_registers           : ipb_reg_v(N_STAT_HIST - 1 downto 0) := (others => (others => '0'));
    signal control_registers          : ipb_reg_v(N_CTRL_HIST - 1 downto 0) := (others => (others => '0'));

    signal address_trigger_window_lower           : std_logic_vector(31 downto 0)            := (others => '0');
    signal address_trigger_window_upper           : std_logic_vector(3 downto 0)             := (others => '0');
    signal address_trigger_window                 : std_logic_vector(36 - 1 downto 0)        := X"0ffffffff";
    signal address_max_value0, address_max_value1 : std_logic_vector(bin_width - 1 downto 0) := (others => '0');
    signal address_histogram_reset                : std_logic                                := '0';
    signal hist0_stub, hist1_stub                 : lword                                    := LWORD_NULL;

    signal occupancy_trigger_window_lower             : std_logic_vector(31 downto 0)            := (others => '0');
    signal occupancy_trigger_window_upper             : std_logic_vector(3 downto 0)             := (others => '0');
    signal occupancy_trigger_window                   : std_logic_vector(36 - 1 downto 0)        := X"0ffffffff";
    signal occupancy_max_value0, occupancy_max_value1 : std_logic_vector(bin_width - 1 downto 0) := (others => '0');
    signal occupancy_histogram_reset                  : std_logic                                := '0';
    signal occupancy_words                            : ldata(cNumberOfCICs - 1 downto 0)        := (others => LWORD_NULL);

begin

    --==============================--
    -- IPBus fabric
    --==============================--

    --==============================--
    fabric : entity work.ipbus_fabric_sel
    --==============================--
    generic map (
        NSLV      => N_SLAVES,
        SEL_WIDTH => IPBUS_SEL_WIDTH
    )
    port map (
        ipb_in          => ipb_in,
        ipb_out         => ipb_out,
        sel             => ipbus_sel_linkmonitor(ipb_in.ipb_addr),
        ipb_to_slaves   => ipb_to_slaves,
        ipb_from_slaves => ipb_from_slaves
    );

    --==============================--
    histogram_ctrl : entity work.ipbus_ctrlreg_v
    --==============================--
    generic map (
        N_CTRL => N_CTRL_HIST,
        N_STAT => N_STAT_HIST
    )
    port map (
        clk       => clk,
        reset     => rst,
        ipbus_in  => ipb_to_slaves(N_SLV_CSR),
        ipbus_out => ipb_from_slaves(N_SLV_CSR),
        d         => status_registers,
        q         => control_registers
    );

    status_registers(0)(bin_width - 1 downto 0) <= address_max_value0;
    status_registers(1)(bin_width - 1 downto 0) <= address_max_value1;
    status_registers(2)(32 - 1 downto 0)        <= occupancy_max_value0;
    status_registers(3)(32 - 1 downto 0)        <= occupancy_max_value1;


    address_trigger_window_lower <= control_registers(0);
    address_trigger_window_upper <= control_registers(1)(3 downto 0);
    address_trigger_window       <= address_trigger_window_upper & address_trigger_window_lower;

    occupancy_trigger_window_lower <= control_registers(2);
    occupancy_trigger_window_upper <= control_registers(3)(3 downto 0);
    occupancy_trigger_window       <= occupancy_trigger_window_upper & occupancy_trigger_window_lower;

    --==============================--
    pAddressHistogram : process (clk_p) is
    --==============================--
    begin
        if rising_edge(clk_p) then
            if (stub.valid = '1') then
                if (stub.data(50) = '0') then
                    hist0_stub <= stub;
                    hist1_stub <= LWORD_NULL;
                else
                    hist0_stub <= LWORD_NULL;
                    hist1_stub <= stub;
                end if;
            else
                hist0_stub <= LWORD_NULL;
                hist1_stub <= LWORD_NULL;
            end if;
        end if;
    end process pAddressHistogram;

    --==============================--
    AddressHistogramInstanceCIC0 : entity work.IPBusHistogram
    --==============================--
    generic map (
        input_width => input_width,
        bin_width   => bin_width,
        data_offset => data_offset
    )
    port map (
        --- Input Ports ---
        clk_p           => clk_p,
        data_in         => hist0_stub,
        histogram_reset => address_histogram_reset,
        --- Output Ports ---
        max_bin_value   => address_max_value0,
        --- IPBus Ports ---
        clk             => clk,
        rst             => rst,
        ipb_in          => ipb_to_slaves(N_SLV_ADDRESS_MEM0),
        ipb_out         => ipb_from_slaves(N_SLV_ADDRESS_MEM0)
    );

    --==============================--
    AddressHistogramInstanceCIC1 : entity work.IPBusHistogram
    --==============================--
    generic map (
        input_width => input_width,
        bin_width   => bin_width,
        data_offset => data_offset
    )
    port map (
        --- Input Ports ---
        clk_p           => clk_p,
        data_in         => hist1_stub,
        histogram_reset => address_histogram_reset,
        --- Output Ports ---
        max_bin_value   => address_max_value1,
        --- IPBus Ports ---
        clk             => clk,
        rst             => rst,
        ipb_in          => ipb_to_slaves(N_SLV_ADDRESS_MEM1),
        ipb_out         => ipb_from_slaves(N_SLV_ADDRESS_MEM1)
    );

    --==============================--
    AddressHistogramResetter : entity work.HistogramResetter
    --==============================--
    port map (
        --- Input Ports ---
        clk_p           => clk_p,
        trigger_window  => address_trigger_window,
        --- Output Ports ---
        histogram_reset => address_histogram_reset
    );


    --==============================--
    pOccupancyHistogram : process (clk_p) is
    --==============================--
    begin
        if rising_edge(clk_p) then
            for i in 0 to cNumberOfCICs - 1 loop
                if (header_start(i) = '1') then
                    occupancy_words(i).valid <= '1';
                    occupancy_words(i).data(3 downto 0) <= header(i).stub_count(3 downto 0);
                else
                    occupancy_words(i) <= LWORD_NULL;               
                end if;
            end loop ; 
        end if;
    end process pOccupancyHistogram;

    --==============================--
    OccupancyHistogramInstanceCIC0 : entity work.IPBusHistogram
    --==============================--
    generic map (
        input_width => 4,
        bin_width   => 32,
        data_offset => 0
    )
    port map (
        --- Input Ports ---
        clk_p           => clk_p,
        data_in         => occupancy_words(0),
        histogram_reset => occupancy_histogram_reset,
        --- Output Ports ---
        max_bin_value   => occupancy_max_value0,
        --- IPBus Ports ---
        clk             => clk,
        rst             => rst,
        ipb_in          => ipb_to_slaves(N_SLV_OCCUPANCY_MEM0),
        ipb_out         => ipb_from_slaves(N_SLV_OCCUPANCY_MEM0)
    );

    --==============================--
    OccupancyHistogramInstanceCIC1 : entity work.IPBusHistogram
    --==============================--
    generic map (
        input_width => 4,
        bin_width   => 32,
        data_offset => 0
    )
    port map (
        --- Input Ports ---
        clk_p           => clk_p,
        data_in         => occupancy_words(1),
        histogram_reset => occupancy_histogram_reset,
        --- Output Ports ---
        max_bin_value   => occupancy_max_value1,
        --- IPBus Ports ---
        clk             => clk,
        rst             => rst,
        ipb_in          => ipb_to_slaves(N_SLV_OCCUPANCY_MEM1),
        ipb_out         => ipb_from_slaves(N_SLV_OCCUPANCY_MEM1)
    );

    --==============================--
    OccupancyHistogramResetter : entity work.HistogramResetter
    --==============================--
    port map (
        --- Input Ports ---
        clk_p           => clk_p,
        trigger_window  => occupancy_trigger_window,
        --- Output Ports ---
        histogram_reset => occupancy_histogram_reset
    );

end architecture;