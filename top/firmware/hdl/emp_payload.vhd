library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
---
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.ipbus_decode_emp_payload.all;
---
use work.emp_data_types.all;
use work.emp_project_decl.all;
use work.emp_device_decl.all;
use work.emp_ttc_decl.all;
use work.emp_slink_types.all;
---
use work.dtc_link_maps.all;
use work.dtc_constants.all;
use work.dtc_data_types.all;


entity emp_payload is
    port (
        --- Input Ports ---
        clk_p          : in    std_logic;
        clk40          : in    std_logic := '0';
        clk_payload    : in    std_logic_vector(2 downto 0);
        rst_payload    : in    std_logic_vector(2 downto 0);
        rst_loc        : in    std_logic_vector(N_REGION - 1 downto 0);
        clken_loc      : in    std_logic_vector(N_REGION - 1 downto 0);
        ctrs           : in    ttc_stuff_array;
        d              : in    ldata(4 * N_REGION - 1 downto 0);
        backpressure   : in    std_logic_vector(SLINK_MAX_QUADS-1 downto 0);
        --- Output Ports ---
        bc0            : out   std_logic;
        gpio           : out   std_logic_vector(29 downto 0);
        gpio_en        : out   std_logic_vector(29 downto 0);
        q              : out   ldata(4 * N_REGION - 1 downto 0);
        slink_q        : out   slink_input_data_quad_array(SLINK_MAX_QUADS-1 downto 0);
        --- IPBus Ports ---
        clk            : in    std_logic;
        rst            : in    std_logic;
        ipb_in         : in    ipb_wbus;
        ipb_out        : out   ipb_rbus
    );
end entity emp_payload;


architecture rtl of emp_payload is


    -- IPBus fabric

    signal ipb_to_slaves              : ipb_wbus_array(N_SLAVES - 1 downto 0);
    signal ipb_from_slaves            : ipb_rbus_array(N_SLAVES - 1 downto 0);

    signal channel_sel                : ipb_reg_v(0 downto 0);


    -- FE data extraction and monitoring

    signal ipb_chain                  : ipbdc_bus_array(cNumberOfFEModules downto 0);
    signal stubs                      : ldata(cNumberOfFEModules - 1 downto 0);
    signal headers                    : tCICHeaderArray(cNumberOfFEModules*cNumberOfCICs - 1 downto 0) := (others => ('0', (others => '0'), (others => '0'), (others => '0')));
    signal header_start_array         : std_logic_vector(cNumberOfFEModules*cNumberOfCICs - 1 downto 0) := (others => '0');


    -- Daqpath

    signal daq_read                   : tDaqFlagLinkArray(cNumberOfFEModules - 1 downto 0);
    signal daq_data                   : tDaqDataLinkArray(cNumberOfFEModules - 1 downto 0);
    signal daq_empty                  : tDaqFlagLinkArray(cNumberOfFEModules - 1 downto 0);


    -- TCDS & Fast commands

    signal global_fcmd                  : tFastCommand;


begin


    --==============================--
    -- IPBus fabric
    --==============================--

    --==============================--
    fabric : entity work.ipbus_fabric_sel
    --==============================--
    generic map (
        NSLV      => N_SLAVES,
        SEL_WIDTH => IPBUS_SEL_WIDTH
    )
    port map (
        ipb_in          => ipb_in,
        ipb_out         => ipb_out,
        sel             => ipbus_sel_emp_payload(ipb_in.ipb_addr),
        ipb_to_slaves   => ipb_to_slaves,
        ipb_from_slaves => ipb_from_slaves
    );

    --==============================--
    channel_ctrl : entity work.ipbus_reg_v
    --==============================--
    generic map (
        N_REG => 1
    )
    port map (
        clk       => clk,
        reset     => rst,
        ipbus_in  => ipb_to_slaves(N_SLV_FE),
        ipbus_out => ipb_from_slaves(N_SLV_FE),
        q         => channel_sel,
        qmask     => (0 => X"0000007f")
    );

    --==============================--
    channel_select : entity work.ipbus_dc_fabric_sel
    --==============================--
    generic map (
        SEL_WIDTH => 7
    )
    port map (
        clk       => clk,
        rst       => rst,
        sel       => channel_sel(0)(6 downto 0),
        ipb_in    => ipb_to_slaves(N_SLV_FE_CHAN),
        ipb_out   => ipb_from_slaves(N_SLV_FE_CHAN),
        ipbdc_out => ipb_chain(0),
        ipbdc_in  => ipb_chain(cNumberOfFEModules)
    );


    --==============================--
    -- FE data extraction and monitoring
    --==============================--

    --==============================--
    module : for i in 0 to cNumberOfFEModules - 1 generate
    --==============================--

        signal ipb_to_channel   : ipb_wbus;
        signal ipb_from_channel : ipb_rbus;

    begin

        --==============================--
        channel_node : entity work.ipbus_dc_node
        --==============================--
        generic map (
            I_SLV     => i,
            SEL_WIDTH => 7,
            PIPELINE  => false
        )
        port map (
            clk       => clk,
            rst       => rst,
            ipb_out   => ipb_to_channel,
            ipb_in    => ipb_from_channel,
            ipbdc_in  => ipb_chain(i),
            ipbdc_out => ipb_chain(i + 1)
        );

        --==============================--
        LinkInterface : entity work.LinkInterface
        --==============================--
        generic map (
            module_type => "2S"
        )
        port map (
            --- Input Ports ---
            clk_p               => clk_p,
            clk40               => clk40,
            link_in             => d(cDTCInputLinkMap(i)),
            daq_read            => daq_read(i),
            global_fcmd         => global_fcmd,
            --- Output Ports ---
            link_out            => q(cDTCInputLinkMap(i)),
            stub_out            => stubs(i),
            header_out          => headers(cNumberOfCICs*(i + 1) - 1 downto cNumberOfCICs*i),
            daq_out             => daq_data(i),
            daq_empty           => daq_empty(i),
            --- IPBus Ports ---
            clk                 => clk,
            rst                 => rst,
            ipb_in              => ipb_to_channel,
            ipb_out             => ipb_from_channel,
            --- Debug Ports ---
            debug_header_start  => header_start_array(cNumberOfCICs*(i + 1) - 1 downto cNumberOfCICs*i)
        );

    end generate module;


    --==============================--
    -- Global TCDS
    --==============================--

    --==============================--
    GlobalFastCommand: entity work.GlobalFastCommand
    --==============================--
    port map(
        --- Input Ports ---
        clk_p            => clk_p,
        clk40            => clk40,
        --- Output Ports ---
        global_fcmd      => global_fcmd,
        --- IPBus Ports ---
        clk              => clk,                       
        rst              => rst,                       
        ipb_in           => ipb_to_slaves(N_SLV_TCDS_FAST_CMD),  
        ipb_out          => ipb_from_slaves(N_SLV_TCDS_FAST_CMD)
    );




    --==============================--
    -- L1 Data Aggregator
    --==============================--

    --==============================--
    L1DataAggregator : entity work.L1DataAggregator
    --==============================--
    port map (
        --- Input Ports ---
        clk_p   => clk_p,
        daq_in  => daq_data,
        empty   => daq_empty,
        --- Output Ports ---
        read    => daq_read,
        --- IPBus Ports ---
        clk     => clk,
        rst     => rst,
        ipb_in  => ipb_to_slaves(N_SLV_BE_DAQ),
        ipb_out => ipb_from_slaves(N_SLV_BE_DAQ)
    );

    --==============================--
    pRouteStubsToOutput : process (clk_p) is
    --==============================--
    begin
        if rising_edge(clk_p) then
            for i in 0 to cNumberOfOutputLinks - 1 loop
                q(cDTCOutputLinkMap(i)).valid  <= stubs(i).valid;
                q(cDTCOutputLinkMap(i)).data   <= stubs(i).data;
                q(cDTCOutputLinkMap(i)).strobe <= '1';
            end loop;
        end if;
    end process pRouteStubsToOutput;

    --==============================--
    LinkMonitorInterfaceInstance : entity work.LinkMonitorInterface
    --==============================--
    generic map (
        N_LINKS => 4
    )
    port map (
        --- Input Ports ---
        clk_p => clk_p,
        stubs => stubs(3 downto 0),
        headers => headers(7 downto 0),
        header_start => header_start_array(7 downto 0),
        --- IPBus Ports ---
        clk     => clk,
        rst     => rst,
        ipb_in  => ipb_to_slaves(N_SLV_LINK_MONITOR),
        ipb_out => ipb_from_slaves(N_SLV_LINK_MONITOR)
    );



    bc0     <= '0';
    gpio    <= (others => '0');
    gpio_en <= (others => '0');


    --==============================--
    slink_q_gen : for q in SLINK_MAX_QUADS-1 downto 0 generate
    --==============================--
    begin
        slink_q(q) <= SLINK_INPUT_DATA_ARRAY_NULL;
    end generate slink_q_gen;


end architecture rtl;
